package com.paradigma.bd4bfstreaming.domain

/**
 * Created by paradigma
 */
case class BD4BFModel(date: Long,
                      weights: Seq[Double],
                      intercept: Double,
                      numFeatures: Int,
                      numClasses: Int) extends Ordered[BD4BFModel] {
  override def compare(that: BD4BFModel): Int = ???
}