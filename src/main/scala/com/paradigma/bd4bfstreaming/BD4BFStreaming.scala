package com.paradigma.bd4bfstreaming

import java.time.{Instant, LocalDateTime, ZoneOffset}
import java.util.TimeZone

import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector.streaming._
import com.paradigma.aggregations.Aggregations
import com.paradigma.aggregations.domain.{DailyAggregation, MonthlyAggregation, Transaction, TrimonthlyAggregation}
import com.paradigma.bd4bfstreaming.domain.{BD4BFModel, BD4BFStandardScalerModel, TransactionFeatures}
import main.scala.com.paradigma.bd4bfstreaming.utils.ConfigUtils
import org.apache.flume.source.avro.AvroFlumeEvent
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.mllib.classification.LogisticRegressionModel
import org.apache.spark.mllib.feature.StandardScalerModel
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.apache.spark.streaming.flume._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.json4s._
import org.json4s.jackson.Serialization.read


/**
  * Created by paradigma
  */
object BD4BFStreaming {

  // remove comments to anonymized transactions
  //  private val FieldsToAnonymize: List[String] = List[String]("country")

  /*private val defaultMaster = "local[*]"
  private val appName = "BD4BFStreaming"
  private val appId = "BD4BFStreaming"
  private val defaultFlumeHost = "localhost"
  private val defaultFlumePort = 4444
*/

  implicit val formats = org.json4s.jackson.Serialization.formats(NoTypeHints)

  def main(args: Array[String]) {

    println("Starting application BD4BFStreaming...")
    val config = new ConfigUtils(args(0))
    val master = config.getSparkMaster()
    val flumeHost = config.getFlumeHost()
    val flumePort = config.getFlumePort()
    val cassandraHost = config.getCassandraHost()
    val sparkCWTime = config.getSparkCWTime()
    val appName = config.getAppName()
    val appId = config.getAppId()
    val cassandraKeyspace = config.getCassandraKeyspace()
    val modelTableName = config.getModelTableName()
    val scalerModelName = config.getScalerModelTable()

    //Setting coarse log level for spark
    Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

    val sparkConf: SparkConf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)
      .set("spark.app.id", appId)
      .set("spark.cassandra.connection.host", cassandraHost)
      // use Kryo serializer to improve performance (10x faster than default Java serializer)
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")

    // create streaming context
    val ssc: StreamingContext = new StreamingContext(sparkConf, sparkCWTime)

    // get model from C*
    val bd4bfModelMaxDate: Long =
      ssc.cassandraTable[Long](cassandraKeyspace, modelTableName).select("date").max
    val bd4bfModel: BD4BFModel =
      ssc.cassandraTable[BD4BFModel](cassandraKeyspace, modelTableName).where("date = ?", bd4bfModelMaxDate).first

    if (bd4bfModel == null) {
      //println("Model no present, impossible to start")
      throw new RuntimeException("Model not present, impossible to start")
    }

    val logisticRegressionModel: LogisticRegressionModel =
      new LogisticRegressionModel(
        Vectors.dense(bd4bfModel.weights.toArray),
        bd4bfModel.intercept,
        bd4bfModel.numFeatures,
        bd4bfModel.numClasses)

    // get scalerModel from C*
    val standardScalerModelMaxDate: Long =
      ssc.cassandraTable[Long](cassandraKeyspace, scalerModelName ).select("date").max
    val bd4bfStandardScalerModel: BD4BFStandardScalerModel =
      ssc.cassandraTable[BD4BFStandardScalerModel](cassandraKeyspace, scalerModelName).where("date = ?", standardScalerModelMaxDate).first

    if (bd4bfStandardScalerModel == null) {
      println("Standard Scaler Model no present, impossible to start")
      throw new RuntimeException("Standard Scaler Model no present, impossible to start")
    }

    val standardScalerModel =
      new StandardScalerModel(Vectors.dense(bd4bfStandardScalerModel.std.toArray), Vectors.dense(bd4bfStandardScalerModel.mean.toArray), true, false)

    // create stream
    val dStream: ReceiverInputDStream[SparkFlumeEvent] = FlumeUtils.createPollingStream(ssc, flumeHost, flumePort)

    // Print out the count of events received from this server in each batch
    dStream.count.map(cnt => "The number of received events is " + cnt.toString).print


    val anonymizedTransactionStream: DStream[Transaction] = dStream.map(sparkFlumeEvent => {

      val avroFlumeEvent: AvroFlumeEvent = sparkFlumeEvent.event

      // remove comments to anonymized transactions
      val txn: Map[String, String] =
      /*anonymizeTransaction(*/ read[Map[String, String]](new String(avroFlumeEvent.getBody.array)) /*)*/

      // anonymized transaction
      new Transaction(
        txn.getOrElse("ccnumber", ""),
        txn.getOrElse("txnid", ""),
        txn.getOrElse("amount", "").toDouble,
        txn.getOrElse("date", "").toLong,
        txn.getOrElse("ecommerce", "").toInt,
        txn.getOrElse("foreign", "").toInt,
        txn.getOrElse("fraud", "").toInt, // fraud field always false, not used
        txn.getOrElse("merchant", ""),
        txn.getOrElse("country", ""))
    })

    val Merchants = config.getItemsNamedMerchants()
    val Countries = config.getItemsNamedCountries()

    val txnFeatureVectorStream: DStream[(String, Vector)] =
      anonymizedTransactionStream.transform(rdd => {

        // get three months ago date
        val now: LocalDateTime =
          LocalDateTime.ofInstant(Instant.ofEpochSecond(System.currentTimeMillis / 1000), TimeZone.getDefault.toZoneId)
        val threeMonthsAgo: Long = now.minusDays(91).toEpochSecond(ZoneOffset.UTC)

        val transactionsFromCassandra: RDD[Transaction] =
          ssc.cassandraTable[Transaction](cassandraKeyspace, "anonymized_txn_streaming").where("date >= ?", threeMonthsAgo)
        println("The number of retrieved transactions is " + transactionsFromCassandra.count)

        val txnsFromCassandraByCreditCard: RDD[(String, Iterable[Transaction])] =
          transactionsFromCassandra.keyBy(txn => txn.ccnumber).groupByKey

        val batchTransactionsByCreditCard: RDD[(String, Iterable[Transaction])] =
          rdd.keyBy(txn => txn.ccnumber).groupByKey

        val transactionsWithFeatures: RDD[TransactionFeatures] =
          batchTransactionsByCreditCard.leftOuterJoin(txnsFromCassandraByCreditCard)
            .flatMap {
              case (_, (txnsFromCsv, txnsFromCassandra)) =>
                txnsFromCsv.map(transaction =>
                  (transaction, Seq.concat(txnsFromCsv.toSeq, txnsFromCassandra match {
                    case Some(iterable) => iterable.toSeq
                    case None => Seq.empty
                  }))
                )
            }.map({ case (txn, relatedTransactions) => aggregateTransaction(txn, relatedTransactions) })



        // aggregate and generate
        transactionsWithFeatures
          .map(txn => (txn.txnid, Vectors.dense(Array(
            txn.amount,
            txn.amountOverMonth,
            txn.amountSameDay,
            txn.averageDailyOverMonth,
            txn.amountMerchantTypeOverMonth(Merchants.head),
            txn.amountMerchantTypeOverMonth(Merchants(1)),
            txn.amountMerchantTypeOverMonth(Merchants(2)),
            txn.amountMerchantTypeOverMonth(Merchants(3)),
            txn.numberMerchantTypeOverMonth(Merchants.head).toDouble,
            txn.numberMerchantTypeOverMonth(Merchants(1)).toDouble,
            txn.numberMerchantTypeOverMonth(Merchants(2)).toDouble,
            txn.numberMerchantTypeOverMonth(Merchants(3)).toDouble,
            txn.amountSameCountryOverMonth(Countries.head),
            txn.amountSameCountryOverMonth(Countries(1)),
            txn.amountSameCountryOverMonth(Countries(2)),
            txn.amountSameCountryOverMonth(Countries(3)),
            txn.amountSameCountryOverMonth(Countries(4)),
            txn.amountSameCountryOverMonth(Countries(5)),
            txn.numberSameCountryOverMonth(Countries.head).toDouble,
            txn.numberSameCountryOverMonth(Countries(1)).toDouble,
            txn.numberSameCountryOverMonth(Countries(2)).toDouble,
            txn.numberSameCountryOverMonth(Countries(3)).toDouble,
            txn.numberSameCountryOverMonth(Countries(4)).toDouble,
            txn.numberSameCountryOverMonth(Countries(5)).toDouble,
            txn.averageOverThreeMonths,
            txn.ecommerce.toDouble,
            txn.foreign.toDouble,
            txn.numberSameDay.toDouble,
            txn.amountMerchantTypeOverThreeMonths(Merchants.head),
            txn.amountMerchantTypeOverThreeMonths(Merchants(1)),
            txn.amountMerchantTypeOverThreeMonths(Merchants(2)),
            txn.amountMerchantTypeOverThreeMonths(Merchants(3))
          ))))
      })

    // save anonymized transactions for aggregations
    anonymizedTransactionStream.foreachRDD(x => {
      x.foreach(transaction => {
        CassandraConnector(new SparkConf()
          .setMaster(master)
          .setAppName(appName)
          .set("spark.app.id", appId)
          .set("spark.cassandra.connection.host", cassandraHost)).withSessionDo {
          val insertQuery = "insert into bd4bf.anonymized_txn_streaming (ccnumber, txnid, amount, date, ecommerce, foreign, fraud, merchant, country) values ('" +
            transaction.ccnumber + "', '" + transaction.txnid + "', " + transaction.amount + ", " + transaction.date + ", " +
            transaction.ecommerce + ", " + transaction.foreign + ", " + transaction.fraud + ", '" + transaction.merchant + "', '" + transaction.country + "' )"
          session => session.execute(insertQuery)
            session.close()
        }
      })
    })

    // save classification of every transaction
    val classifiedTransactionsStream: DStream[(String, Double)] =
      txnFeatureVectorStream.map(vectorRdd => (vectorRdd._1, logisticRegressionModel.predict(standardScalerModel.transform(vectorRdd._2))))

    classifiedTransactionsStream.foreachRDD(x => {
      x.foreach(transaction => {
        CassandraConnector(new SparkConf()
          .setMaster(master)
          .setAppName(appName)
          .set("spark.app.id", appId)
          .set("spark.cassandra.connection.host", "localhost")).withSessionDo {
          val insertQuery = "insert into bd4bf.txn_classification (txnid, fraud) values ('" + transaction._1 + "', " + transaction._2.toInt + ")"
          session => session.execute(insertQuery)
            session.close()
        }
      })
    })

    ssc.start
    ssc.awaitTermination

  }

  // remove comments to anonymized transactions
  //  private def anonymizeTransaction(txn: Map[String, String]): Map[String, String] = {
  //    var anonymizedTxn: Map[String, String] = Map.empty[String, String]
  //    try {
  //      anonymizedTxn = PartialAnonymization.anonymize(txn, FieldsToAnonymize)
  //    } catch {
  //      case iae: IllegalArgumentException =>
  //        println("Error anonymizing transaction (" + iae.getMessage + ") ", txn.mkString("|"))
  //      case e: Exception =>
  //        println("Unknown error anonymizing transaction (" + e.getMessage + ") ", txn.mkString("|"))
  //    }
  //    anonymizedTxn
  //  }

  private def aggregateTransaction(txn: Transaction, anonymizedValidTransactions: Seq[Transaction]): TransactionFeatures = {
    val endTimestamp: Long = txn.date
    val endDate: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(endTimestamp), TimeZone.getDefault.toZoneId)

    // three months aggregations
    val threeMonthStart: Long = endDate.minusMonths(3).toEpochSecond(ZoneOffset.UTC)
    val threeMonthTxns: Seq[Transaction] =
      anonymizedValidTransactions.filter(line => line.date >= threeMonthStart && line.date <= endTimestamp)
    val threeMonthAggregations: TrimonthlyAggregation = Aggregations.calculateTrimonthlyAggregations(threeMonthTxns)

    // one month aggregations
    val oneMonthStart: Long = endDate.minusMonths(1).toEpochSecond(ZoneOffset.UTC)
    val oneMonthTxns: Seq[Transaction] = threeMonthTxns.filter(line => line.date >= oneMonthStart && line.date <= endTimestamp)
    val oneMonthAggregations: MonthlyAggregation = Aggregations.calculateMonthlyAggregations(oneMonthTxns)

    // one day aggregations
    val oneDayStart: Long = endDate.minusHours(24).toEpochSecond(ZoneOffset.UTC)
    val oneDayTxns: Seq[Transaction] = oneMonthTxns.filter(line => line.date >= oneDayStart && line.date <= endTimestamp)
    val oneDayAggregations: DailyAggregation = Aggregations.calculateDailyAggregations(oneDayTxns)

    transactionFeaturesBuilder(txn, oneDayAggregations, oneMonthAggregations, threeMonthAggregations)
  }

  private def transactionFeaturesBuilder(txn: Transaction, dailyAggregation: DailyAggregation,
                                         monthlyAggregation: MonthlyAggregation,
                                         trimonthlyAggregation: TrimonthlyAggregation): TransactionFeatures = {
    new TransactionFeatures(txn.ccnumber, txn.txnid, txn.amount, txn.date, txn.ecommerce, txn.foreign, txn.fraud, txn.merchant, txn.country,
      dailyAggregation.amountSameDay, dailyAggregation.numberSameDay, monthlyAggregation.amountOverMonth,
      monthlyAggregation.averageDailyOverMonth, monthlyAggregation.amountMerchantTypeOverMonth,
      monthlyAggregation.numberMerchantTypeOverMonth, monthlyAggregation.amountSameCountryOverMonth, monthlyAggregation.numberSameCountryOverMonth,
      trimonthlyAggregation.averageOverThreeMonths, trimonthlyAggregation.amountMerchantTypeOverThreeMonths)
  }

}
