package com.paradigma.bd4bfstreaming.domain

/**
 * Created by paradigma
 */
case class TransactionFeatures(ccnumber: String,
                          txnid: String,
                          amount: Double,
                          date: Long,
                          ecommerce: Int,
                          foreign: Int,
                          fraud: Int,
                          merchant: String,
                          country: String,
                          amountSameDay: Double,
                          numberSameDay: Long,
                          amountOverMonth: Double,
                          averageDailyOverMonth: Double,
                          amountMerchantTypeOverMonth: Map[String, Double],
                          numberMerchantTypeOverMonth: Map[String, Int],
                          amountSameCountryOverMonth: Map[String, Double],
                          numberSameCountryOverMonth: Map[String, Int],
                          averageOverThreeMonths: Double,
                          amountMerchantTypeOverThreeMonths: Map[String, Double]
                           ) extends Serializable {
}
