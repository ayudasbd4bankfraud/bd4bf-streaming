package com.paradigma.bd4bfstreaming.domain

/**
 * Created by paradigma
 */
case class BD4BFStandardScalerModel(date: Long,
                                    std: List[Double],
                                    mean: List[Double]) {}