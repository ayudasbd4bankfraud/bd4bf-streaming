package com.paradigma.bd4bfstreaming.utils

import java.io.File
import scala.collection.JavaConversions._
import com.typesafe.config.{ConfigFactory, Config}


class ConfigReader(config: Config) {

  def this(path: String) {
    this(ConfigFactory.load(ConfigFactory.parseFile(new File(path))))
  }

  def getOrElse(key: String, default: String): String ={
    if(config.hasPath(key)){
      config.getString(key)
    }else{
      default
    }
  }

  def getStringListOrElse(key: String, default:List[String]): List[String] = {
    if(config.hasPath(key)){
      config.getStringList(key).toList
    }else{
      default
    }
  }

  def getIntOrElse(key: String, default: Int): Int = {
    if(config.hasPath(key)){
      config.getInt(key)
    }else{
      default
    }
  }



}
