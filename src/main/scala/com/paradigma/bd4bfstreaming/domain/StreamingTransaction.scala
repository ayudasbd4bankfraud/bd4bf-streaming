package com.paradigma.bd4bfstreaming.domain

/**
  * Created by paradigma
  */
case class StreamingTransaction(val ccnumber: String,
                                val txnid: String,
                                val amount: Double,
                                val date: Long,
                                val ecommerce: Int,
                                val foreign: Int,
                                val fraud: Int,
                                val merchant: String,
                                val country: String,
                                val processed: Int
                               ) extends Serializable {}