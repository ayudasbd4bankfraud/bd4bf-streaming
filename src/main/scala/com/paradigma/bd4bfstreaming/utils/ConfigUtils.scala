package main.scala.com.paradigma.bd4bfstreaming.utils
import com.paradigma.bd4bfstreaming.utils.ConfigReader
import org.apache.flume.source.avro.AvroFlumeEvent
import org.apache.log4j.{Level, Logger}
import org.apache.spark.streaming.{Seconds, StreamingContext}

class ConfigUtils(configReader: ConfigReader) {
  private val defaultMaster = "local[*]"
  private val appName = "BD4BFStreaming"
  private val appId = "BD4BFStreaming"
  private val defaultFlumeHost = "localhost"
  private val cassandraHost = "localhost"
  private val defaultFlumePort = 4444
  private val cWTime = 5

  val Merchants = List[String]("0742", "0763", "1520", "0780")

  val Countries = List[String]("FR", "US", "ES", "GB", "GR", "CH")
  

  def this(path: String) {
    this(new ConfigReader(path))
  }

  def getCassandraHost(): String = {
    configReader.getOrElse("cassandraHost", cassandraHost)
  }

  def getFlumePort(): Int ={
    configReader.getIntOrElse("flumePort", defaultFlumePort)
  }

  def getSparkMaster(): String ={
    configReader.getOrElse("sparkMaster", defaultMaster)
  }

  def getFlumeHost(): String ={
    configReader.getOrElse("flumeHost", defaultFlumeHost)
  }

  def getAppName(): String = {
    configReader.getOrElse("appName", appName)
  }

  def getAppId(): String = {
    configReader.getOrElse("appId", appId)
  }

  def getItemsNamedCountries() : List[String] = {
    configReader.getStringListOrElse("countries", Countries)
  }

  def getItemsNamedMerchants() : List[String] = {
    configReader.getStringListOrElse("merchants", Merchants)
  }

  def getSparkCWTime()= {
    Seconds(configReader.getIntOrElse("cWTime", cWTime))
  }

  def getCassandraKeyspace() = {
    "bd4bf"
  }

  def getModelTableName() = {
    "logistic_regression_model"
  }

  def getScalerModelTable() = {
    "standard_scaler_model"
  }

}
